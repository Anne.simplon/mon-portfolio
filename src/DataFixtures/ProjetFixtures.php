<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Projet;
use App\Entity\Skill;

class ProjetFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        for($i = 0; $i <= 8; $i++) {
            $skill = new Skill();
            $skill->setLanguage($faker->sentence());

            $manager->persist($skill);
        }

        for($j =1; $j<= 8; $j++){
            $projet = new Projet();

            $content = '<p>' .join($faker->paragraphs(1), '</p><p>') . '</p>';

            $projet->setTitle($faker->sentence())
                   ->setContent($content)
                   ->setLink("https://gitlab.com/gitlab-org")
                   ->addSkill($skill);

            $manager->persist($projet);

        }

        $manager->flush();
    }
}
