<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;

use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Projet;
use App\Repository\ProjetRepository;
use App\Form\ProjetType;


class PortfolioController extends AbstractController
{
    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function index(ProjetRepository $repo)
    {
        $projets = $repo->findAll();

        return $this->render('portfolio/index.html.twig', [
            'controller_name' => 'PortfolioController',
            'projets' => $projets 
        ]);
    }


    /**
     * @Route("/", name="home")
     */
    public function home() 
    {
        return $this->render('portfolio/home.html.twig', [
            'title' => 'Mon Portfolio',
        ]);
    }


     /**
     * @Route("/portfolio/new", name="portfolio_create")
     * @Route("/portfolio/{id}/edit", name="portfolio_edit")
     */
    public function form(Projet $projet = null, Request $request, ObjectManager $manager){

        if(!$projet) { 
        $projet = new Projet();
        }
        
        // $projet->setTitle("Titre exemple")
        //         ->setContent("Description exemple");

        // $form = $this->createFormBuilder($projet)
        //              ->add('title')
        //              ->add('content')
        //              ->add('link')
        //              ->getForm();

        $form = $this->createForm(ProjetType::class, $projet);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($projet);
            $manager->flush();

            return $this->redirectToRoute('portfolio_show', ['id' => $projet->getId()]);
        }

        return $this->render('portfolio/create.html.twig', [
            'formProjet' => $form->createView()
        ]);
    }

    /**
     * @Route("/portfolio/{id}", name="portfolio_show")
     */
    public function show(Projet $projet) 
    {

        return $this->render('portfolio/show.html.twig', [
            'projet' => $projet 
        ]
         );
    }

}
